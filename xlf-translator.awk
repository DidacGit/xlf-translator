#!/usr/bin/awk -f
# Translate XLF files using DeepL's API.

# Usage: xlf-translator SRC_FILES
#
#   - SRC_FILES: one or more XLF files to translate. Assumes they are always English.
#
# Environment variables:
#
#   - DEEPL_URL and DEEPL_KEY: URL and key to use the DeepL API.
#     If they are missing, it will check for the environment variables with these names.
#
#   - XLF_LANGS: list of the ISO code of the languages to translate the source to.
#     Defaults to "de fr it es pt".

BEGIN {
    # Default languages to translate to.
    default_langs = "de fr it es pt"

    # DeepL Formality parameter for some target languages.
    formality["default"] = "default"
    formality["de"] = "prefer_more"
    formality["fr"] = "prefer_more"
    formality["it"] = "prefer_less"
    formality["es"] = "prefer_less"
    formality["pt"] = "prefer_more"

    # Get the environment variables.
    deepl_url = ENVIRON["DEEPL_URL"]
    deepl_key = ENVIRON["DEEPL_KEY"]
    if (!deepl_key || !deepl_url) {
        print "There are missing DEEPL parameters"
        exit
    }
    # Use the default language list if the variable is not passed.
    langs = ENVIRON["XLF_LANGS"]
    if (!langs)
        langs = default_langs
        # Transform the list into an array.
    split(langs, langs_array)

    # Make the record separator be this tag, to iterate them instead of the lines.
    RS = "<trans-unit +id="
    print "Processing files..."
}

# Process the file, storing each source tag into an associative array:
# source_records[i] = [key: KEY, value: VALUE].

# We need to use an indexed array instead of a simple assoc array to be able to retrieve the values
# in the same order.
{
    if(match($0, "^\".+\">")) {
        key = substr($0, RSTART + 1, RLENGTH - 3)
        if (match($0, "<source>.+</source>")) {
            value = substr($0, RSTART + 8, RLENGTH - 17)
            # Transform newlines (and any blank space around them) into spaces.
            gsub(/[ \t]*\n[ \t]*/, " ", value)

            # Avoid the version record.
            if (key != "setup.version") {
                record_count++
                source_records[record_count]["key"] = key
                source_records[record_count]["value"] = value
            }
        }
    }
}

END {
    # Reset the record separator.
    RS="\n"

    # Iterate the languages.
    for (i in langs_array) {
        lang_code = langs_array[i]
        target_file = lang_code "." FILENAME

        # Check if the file already exists. If it does, prompt to delete it.
        if (system("test -f " target_file) == 0) {
            printf("File: '" target_file "' already exists, overwrite? (y/n) ")
            getline reply < "-"
            if (reply == "y") {
                system("rm "target_file)
                printf("File '" target_file "' was deleted.\n")
            }
        }

        printf("Translating to file: '" target_file "'... \n")

        # Iterate the original records and print out the tags to the target file.
        for (i = 1; i <= record_count; i++) {
            key = source_records[i]["key"]
            value = source_records[i]["value"]
            printf("Translating: %s\n", key)
            translation = translate_text(value, lang_code)
            print "<trans-unit id=\"" key "\">\n" \
                  "\t<source>"value"</source>\n" \
                  "\t<target>"translation"</target>\n" \
                  "</trans-unit>\n" > target_file
        }
        printf("done.\n\n")
    }
}

# Translate the passed TEXT from English to the passed TARGET_LANG (ISO code).
function translate_text(text, target_lang,    cmd, output, form) {

    # Configure the formality for the translation.
    if (length(formality[target_lang]) == 0)
        form = formality["default"]
    else
        form = formality[target_lang]

    cmd = "curl -X POST	'" deepl_url "'" \
          " -H 'Authorization: DeepL-Auth-Key " deepl_key "'" \
          " -d \"text=" text "\"" \
          " -d 'formality=" form "'" \
          " -d 'target_lang=" target_lang "' 2> /dev/null"
    cmd | getline output
    close(cmd)

    # Trim all the text not related to the translation.
    gsub(/^.*"text": *"/, "", output)
    gsub(/["}\]]+ *$/, "", output)
    return output
}
